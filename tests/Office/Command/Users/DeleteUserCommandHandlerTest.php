<?php

namespace App\Tests\Office\Command\Users;

use App\Office\Application\Command\Users\DeleteUserCommand;
use App\Office\Application\Command\Users\DeleteUserCommandHandler;
use App\Shared\Domain\Model\Users\UserRepository;
use App\Shared\Domain\Pagination;
use App\Tests\Double\Shared\User\UserResponseRepositoryStub;
use App\Tests\Double\Shared\User\UserStub;
use PHPUnit\Framework\TestCase;

class DeleteUserCommandHandlerTest extends TestCase
{
    private DeleteUserCommandHandler $handler;
    private UserRepository $repository;
    private array $users;

    protected function setUp(): void
    {
        $this->users = [];
        for($n=1; $n<=10; $n++) {
            $this->users[] = UserStub::create($n);
        }
        $this->repository = UserResponseRepositoryStub::deleteUser($this->users);
        $this->handler = new DeleteUserCommandHandler($this->repository);

        parent::setUp();
    }

    public function test_delete_user(): void
    {
        $idToDelete = $this->users[rand(1,10)]->getId();
        $pagination = new Pagination(1);
        $findAll = $this->repository->findAll($pagination);

        $this->assertEquals(10, $findAll->count());

        $command = new DeleteUserCommand($idToDelete);
        $userFound = call_user_func($this->handler, $command);

        $this->assertTrue($userFound);
        $userFind = $this->repository->findUserById($idToDelete);
        $findAll = $this->repository->findAll($pagination);

        $this->assertEquals(9, $findAll->count());
        $this->assertNull($userFind);
    }
}