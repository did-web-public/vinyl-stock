<?php

namespace App\Tests\Office\Command\Users;

use App\Office\Application\Command\Users\EditUserCommand;
use App\Office\Application\Command\Users\EditUserCommandHandler;
use App\Shared\Domain\Model\Users\User;
use App\Shared\Domain\Model\Users\UserRepository;
use App\Tests\Double\Shared\User\UserResponseRepositoryStub;
use App\Tests\Double\Shared\User\UserStub;
use PHPUnit\Framework\TestCase;

class EditUserCommandHandlerTest extends TestCase
{
    private UserRepository $repository;
    private EditUserCommandHandler $handler;
    private string $nameExpected = 'Eduard Pinuaga';
    private User $userOriginal;

    protected function setUp(): void
    {
        $this->userOriginal = UserStub::create();
        $this->repository = UserResponseRepositoryStub::updateUser([$this->userOriginal]);
        $this->handler = new EditUserCommandHandler($this->repository);
    }

    public function test_edit_user(): void
    {
        $userBeforeChanged = $this->repository->findUserById($this->userOriginal->getId());
        $this->assertEquals($this->userOriginal->getId(), $userBeforeChanged->id());
        $this->assertEquals($this->userOriginal->getName(), $userBeforeChanged->name());

        $command = new EditUserCommand(
                $this->userOriginal->getId(),
                $this->userOriginal->getEmail(),
                $this->nameExpected,
                $this->userOriginal->getRoles()
        );

        call_user_func($this->handler, $command);

        $userAfterChanged = $this->repository->findUserById($this->userOriginal->getId());
        $this->assertEquals($this->userOriginal->getId(), $userAfterChanged->id());
        $this->assertEquals($this->nameExpected, $userAfterChanged->name());
        $this->assertNotEquals('Ratoncito Perez', $userAfterChanged->name());

    }
}