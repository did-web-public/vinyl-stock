<?php

namespace App\Tests\Office\Command\Users;

use App\Office\Application\Command\Users\CreateUserCommand;
use App\Office\Application\Command\Users\CreateUserCommandHandler;
use App\Shared\Domain\Model\Users\UserRepository;
use App\Tests\Double\Shared\User\UserResponseRepositoryStub;
use App\Tests\Double\Shared\User\UserStub;
use PHPUnit\Framework\TestCase;

class CreateUserCommendHandlerTest extends TestCase
{
    private CreateUserCommandHandler $handler;
    private UserRepository $repository;

    protected function setUp(): void
    {
        $this->repository = UserResponseRepositoryStub::empty();
        $this->handler = new CreateUserCommandHandler($this->repository);

        parent::setUp();
    }

    public function test_create_user(): void
    {
        $userRandom = UserStub::create(1);

        $command = new CreateUserCommand(
                $userRandom->getEmail(),
                $userRandom->getName(),
                $userRandom->getRoles(),
                $userRandom->getPassword()
        );
       call_user_func($this->handler, $command);


        $userFind = $this->repository->findUserById($userRandom->getId());


        $this->assertEquals($userFind->id(), $userRandom->getId());
        $this->assertEquals($userFind->name(), $userRandom->getName());
    }


}