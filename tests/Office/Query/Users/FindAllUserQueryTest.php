<?php

namespace App\Tests\Office\Query\Users;

use App\Office\Application\Query\Users\FindAllUsers;
use App\Office\Application\Query\Users\FindAllUsersQuery;
use App\Tests\Double\Shared\User\UserResponseRepositoryStub;
use App\Tests\Double\Shared\User\UserStub;
use PHPUnit\Framework\TestCase;

class FindAllUserQueryTest extends TestCase
{
    private FindAllUsersQuery $query;
    private array $randomUsers;

    public function setUp(): void
    {
        $pagination = ['page' => 1];
        $this->query =  new FindAllUsersQuery($pagination);

        $this->randomUsers = [];
        for($n=1; $n<=20; $n++) {
            $this->randomUsers[] = UserStub::create();
        }

        parent::setUp();
    }

    public function test_find_all_users_query(): void
    {
        $SUT = $this->getHandler($this->randomUsers);
        $users = call_user_func($SUT, $this->query);

        $this->assertEquals(20, $users->count());

    }

    private function getHandler($users): FindAllUsers
    {
        return new FindAllUsers(UserResponseRepositoryStub::findAllUsers($users));
    }

}