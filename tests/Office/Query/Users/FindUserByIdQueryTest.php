<?php

namespace App\Tests\Office\Query\Users;

use App\Office\Application\Query\Users\FindUserById;
use App\Office\Application\Query\Users\FindUserByIdQuery;
use App\Tests\Double\Shared\User\UserResponseRepositoryStub;
use App\Tests\Double\Shared\User\UserStub;
use PHPUnit\Framework\TestCase;

class FindUserByIdQueryTest extends TestCase
{

    private array $users;

    public function setUp(): void
    {
        $this->users = [];
        for($n=1; $n<=20; $n++) {
            $this->users[] = UserStub::create($n);
        }

        parent::setUp();
    }

    public function test_find_user_by_id_found(): void
    {
        $idToFind = rand(1,20);
        $query = new FindUserByIdQuery($idToFind);
        $SUT = $this->getHandler($this->users);
        $userFound = call_user_func($SUT, $query);

        $this->assertEquals($idToFind, $userFound->id());

    }

    public function test_find_user_by_id_not_found(): void
    {
        $idToFind = rand(500, 1000);
        $query = new FindUserByIdQuery($idToFind);
        $SUT = $this->getHandler($this->users);
        $userFound = call_user_func($SUT, $query);

        $this->assertNull($userFound);

    }

    public function getHandler($users): FindUserById
    {
        return new FindUserById(UserResponseRepositoryStub::findUserById($users));
    }
}