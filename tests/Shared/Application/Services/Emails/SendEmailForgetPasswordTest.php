<?php

namespace App\Tests\Shared\Application\Services\Emails;

use App\Shared\Application\Services\Emails\EmailForgetPassword;
use App\Shared\Application\Services\Emails\EmailService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;

class SendEmailForgetPasswordTest extends TestCase
{

    public function test_send_mail():void
    {
        $destination = 'info@did-web.com';
        $versionText = 'VERSION v.'.rand(0,1000);

        $symfonyMailer = $this->getMockBuilder(MailerInterface::class)->getMock();
        $symfonyMailer->expects($this->once())->method('send');

        $emailService = new EmailService($symfonyMailer);

        $data = ['destination' => $destination,
                 'urlTarget' => 'TARGET_OK',
                 'version' => $versionText,
                 'emailForm' => 'info@did-web.com' ];

        $email = $emailService->senderEmail(EmailForgetPassword::class, $data);

        $this->assertSame($destination, $email->getFrom()[0]->getAddress());
        $this->assertStringContainsString($versionText, $email->getSubject());
    }
}