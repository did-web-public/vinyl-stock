<?php

namespace App\Tests\Double\Shared\User;

use App\Shared\Domain\Model\Users\User;
use App\Shared\Infrastructure\Helper\Faker;

final class UserStub
{
    private ?int $id;
    private ?string $email;
    private ?array $roles = [];
    private ?string $password;
    private ?string $name;

    public static function create(
            ?int $id = null,
            ?string $email = null,
            ?array $roles = null,
            ?string $password = null,
            ?string $name = null): User
    {
        $rolesOptions = ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN'];

        $user = new User();
        $user->setId($id ?: Faker::number());
        $user->setEmail($email ?: Faker::email());
        $user->setName( $name ?: Faker::firstName() . ' ' . Faker::lastName());
        $user->setRoles( $roles ?: [$rolesOptions[rand(0,2)]]);
        $user->setPassword($password ?: Faker::word());

        return $user;
    }

}