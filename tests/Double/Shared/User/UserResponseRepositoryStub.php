<?php

namespace App\Tests\Double\Shared\User;


use App\Office\Application\Response\Users\UserCollectionResponse;
use App\Office\Application\Response\Users\UserResponse;
use App\Shared\Domain\Model\Users\User;
use App\Shared\Domain\Model\Users\UserRepository;
use App\Shared\Domain\Model\Users\UsersCollection;
use App\Shared\Domain\Pagination;
use Doctrine\Common\Collections\ArrayCollection;

class UserResponseRepositoryStub
{
    public static function empty(): UserRepository
    {
        return self::repository([]);
    }

    public static function findAllUsers(array $data): UserRepository
    {
        return self::repository($data);
    }

    public static function findUserById(array $data): ?UserRepository
    {
        return self::repository($data);
    }

    public static function createUser(): UserRepository
    {
        return self::repository([]);
    }

    public static function deleteUser($data): UserRepository
    {
        return self::repository($data);
    }

    public static function updateUser($data): UserRepository
    {
        return self::repository($data);
    }

    private static function repository(array $data): UserRepository
    {
        return new class($data) implements UserRepository {
            private ArrayCollection $arrayCollection;

            public function __construct(?array $data)
            {
                $this->arrayCollection = new ArrayCollection($data);
            }

            public function findAll(Pagination $pagination): UserCollectionResponse
            {
                $userCollection = UsersCollection::init();
                foreach ($this->arrayCollection as $data) {
                    $userDomain = User::fromInfrastructure($data);
                    $userResponse = new UserResponse($userDomain);
                    $userCollection->add($userResponse);
                }

               return new UserCollectionResponse($userCollection, $pagination);
            }

            public function findUserById(int $idUser): ?UserResponse
            {

                $filterUser = $this->arrayCollection->filter(
                  function (User $user) use ($idUser) {
                      return $user->getId() === $idUser;
                  }
                );

                if(empty($filterUser->first())) {
                    return null;
                }

                $user = $filterUser->first();
                $userDomain = User::fromInfrastructure((object)$user);

                return new UserResponse($userDomain);
            }

            public function updateUser(User $user): void
            {
                $idUser = $user->getId();
                $filter = $this->arrayCollection->filter(
                        function (User $user) use ($idUser) {
                            return (int)$user->getId() === $idUser;
                        }
                );

                if(!empty($filter->first())) {
                    $filter->first()->setEmail($user->getEmail());
                    $filter->first()->setName($user->getName());
                    $filter->first()->setRoles($user->getRoles());
                }

            }

            public function createUser(User $user)
            {
                $user->setId(1);

                if($this->arrayCollection->contains($user)){
                    $this->arrayCollection->removeElement($user);
                    $this->arrayCollection->add($user);
                }else{
                    $this->arrayCollection->add($user);
                }

            }

            public function deleteUser($idUser): bool
            {

                $filterUser = $this->arrayCollection->filter(
                        function (User $user) use ($idUser) {
                            return $user->getId() === $idUser;
                        }
                );

                if ($this->arrayCollection->contains($filterUser->first())){
                    $this->arrayCollection->removeElement($filterUser->first());

                    return true;
                }

                return false;
            }

            public function findUserByEmail(string $email): ?UserResponse
            {
                // TODO: Implement findUserByEmail() method.
            }
        };
    }
}