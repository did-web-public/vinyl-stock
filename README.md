# Viniyl Stock

Gestión de stock para discos de vinilos. COnector con DiscoGS, TodoColección y Prestashop.

# Develop

## Testing

### Testunitarios con PHPunit
Para ejecutar los test unitarios de PHPunit desde el contenedor.

`docker exec -it name_container /bin/bash`

Una vez dentro del contenedor, ejecutar PHPUnit:

`./vendor/bin/phpunit`