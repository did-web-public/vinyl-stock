<?php

namespace App\Shared\Application\Query\Users;

use App\Shared\Domain\Bus\Query\Query;

class FindUserByEmailQuery implements Query
{
    private string $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function email(): string
    {
        return $this->email;
    }
}