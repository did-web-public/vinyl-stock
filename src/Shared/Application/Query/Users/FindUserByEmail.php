<?php

namespace App\Shared\Application\Query\Users;

use App\Office\Application\Response\Users\UserResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Model\Users\UserRepository;

class FindUserByEmail implements QueryHandler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(FindUserByEmailQuery $query): ?UserResponse
    {
        return $this->userRepository->findUserByEmail($query->email());
    }
}