<?php

namespace App\Shared\Application\Command\Users;

use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\Model\ResetPassword\ResetPasswordRepository;
use App\Shared\Domain\Model\Users\User;

class UpdatePasswordCommandHandler implements CommandHandler
{
    private ResetPasswordRepository $resetPasswordRepository;

    public function __construct(ResetPasswordRepository $resetPasswordRepository)
    {
        $this->resetPasswordRepository = $resetPasswordRepository;
    }

    public function __invoke(UpdatePasswordCommand $command): void
    {
        $user =  new User();
        $user->setId($command->id());
        $user->setPassword($command->password());

        $this->resetPasswordRepository->updatePassword($user);
    }
}