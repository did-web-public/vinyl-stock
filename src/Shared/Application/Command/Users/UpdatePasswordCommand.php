<?php

namespace App\Shared\Application\Command\Users;

use App\Shared\Domain\Bus\Command\Command;

class UpdatePasswordCommand implements Command
{
    private int $id;
    private string $password;

    public function __construct(int $id, string $password)
    {
        $this->id = $id;
        $this->password = $password;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function password(): string
    {
        return $this->password;
    }
}