<?php

namespace App\Shared\Application\Command\ResetPassword;

use App\Shared\Domain\Bus\Command\Command;

class CreateResetPasswordCommand implements Command
{
    private string $id;
    private int $userId;
    private string $hashedToken;
    private int $requestAt;
    private int $expiresAt;

    public function __construct(
            string $id,
            int $userId,
            string $hashedToken,
            int $requestAt,
            int $expiresAt
    ) {
        $this->id = $id;
        $this->userId = $userId;
        $this->hashedToken = $hashedToken;
        $this->requestAt = $requestAt;
        $this->expiresAt = $expiresAt;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function userId(): int
    {
        return $this->userId;
    }

    public function hashedToken(): string
    {
        return $this->hashedToken;
    }

    public function requestAt(): int
    {
        return $this->requestAt;
    }

    public function expiresAt(): int
    {
        return $this->expiresAt;
    }
}