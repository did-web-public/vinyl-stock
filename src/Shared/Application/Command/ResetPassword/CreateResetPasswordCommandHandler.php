<?php

namespace App\Shared\Application\Command\ResetPassword;

use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\Model\ResetPassword\ResetPassword;
use App\Shared\Domain\Model\ResetPassword\ResetPasswordRepository;
use App\Shared\Domain\Model\ResetPassword\ValueObjects\ResetPasswordId;

class CreateResetPasswordCommandHandler  implements CommandHandler
{
    private ResetPasswordRepository $resetPasswordRepository;

    public function __construct(ResetPasswordRepository $resetPasswordRepository)
    {
        $this->resetPasswordRepository = $resetPasswordRepository;
    }

    public function __invoke(CreateResetPasswordCommand $command): void
    {
        $resetPassword = new ResetPassword(
                ResetPasswordId::create($command->id()),
                $command->userId(),
                $command->hashedToken(),
                $command->requestAt(),
                $command->expiresAt(),
        );

        $this->resetPasswordRepository->save($resetPassword);
    }

}