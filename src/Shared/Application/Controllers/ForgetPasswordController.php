<?php

namespace App\Shared\Application\Controllers;

use App\Shared\Application\Command\ResetPassword\CreateResetPasswordCommand;
use App\Shared\Application\Query\Users\FindUserByEmailQuery;
use App\Shared\Application\Services\Emails\EmailForgetPassword;
use App\Shared\Application\Services\Emails\EmailService;
use App\Shared\Application\Services\TokenService;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\Model\ResetPassword\ResetPassword;
use App\Shared\Domain\Type\Users\ForgetPasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;

class ForgetPasswordController extends AbstractController
{
    private QueryBus $queryBus;
    private CommandBus $commandBus;
    private TokenService $tokenService;
    private EmailService $emailService;

    public function __construct(
            QueryBus $queryBus,
            CommandBus $commandBus,
            TokenService $tokenService,
            EmailService $emailService
            )
    {
        $this->emailService = $emailService;
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->tokenService = $tokenService;
    }


    public function __invoke(Request $request): Response
    {
        $form = $this->createForm(ForgetPasswordType::class, null, ['method' => 'POST']);
        $success = null;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $email = $form->getData()['email'];
            $user = $this->queryBus->ask(new FindUserByEmailQuery($email));

            if($user === null) {
                $success = 'danger';
                return $this->render('@shared/login/forget_password.html.twig', [
                        'form' => $form,
                        'success' => $success
                ]);
            }

            $token = $this->tokenService->generateRefreshToken($user->id());

            $resetPassword = new ResetPassword(null,
                                                $user->id(),
                                                $token,
                                                $this->tokenService->requestAt(),
                                                $this->tokenService->expiresAt()
            );

            $command = new CreateResetPasswordCommand(
                    $resetPassword->id(),
                    $resetPassword->userId(),
                    $resetPassword->hashedToken(),
                    $resetPassword->requestAt(),
                    $resetPassword->expiresAt()
            );
            $this->commandBus->dispatch($command);

            $this->emailService->senderEmail(EmailForgetPassword::class,
                            ['destination' => $email,
                             'urlTarget' => $request->getScheme() . '://' . $this->getParameter('base.url') .
                                            $this->generateUrl('shared.change.password').'?token=' .
                                            $resetPassword->hashedToken(),
                             'version' => $this->getParameter('app.name'). ' v.'.$this->getParameter('app.version'),
                             'emailForm' => $this->getParameter('email.from')]
            );

            $success = 'success';

        }

        return $this->render('@shared/login/forget_password.html.twig', [
                'form' => $form,
                'success' => $success
                ]);
    }
}