<?php

namespace App\Shared\Application\Controllers;

use App\Office\Application\Command\Users\EditUserCommand;
use App\Office\Application\Query\Users\FindUserByIdQuery;
use App\Shared\Application\Command\Users\UpdatePasswordCommand;
use App\Shared\Application\Services\TokenService;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\Model\Users\User;
use App\Shared\Domain\Type\ResetPassword\ChangePasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class ChangePasswordController extends AbstractController
{

    private QueryBus $queryBus;
    private CommandBus $commandBus;
    private TokenService $tokenService;
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(QueryBus $queryBus,
            CommandBus $commandBus,
            TokenService $tokenService,
            UserPasswordHasherInterface $passwordHasher)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->tokenService = $tokenService;
        $this->passwordHasher = $passwordHasher;
    }

    public function __invoke(Request $request): Response
    {
        $success = 'stepone';
        $form = $this->createForm(ChangePasswordType::class, null, ['method' => 'POST']);
        $form->handleRequest($request);
        $token = $this->tokenService->decodeRefreshToken($request->get('token'));

        if($token === null) {
            $success = 'danger';
            return $this->render('@shared/login/change_password.html.twig', [
                    'form' => $form,
                    'success' => $success,
                    'data' => ''
            ]);
        }

        if ($form->isSubmitted() && $form->isValid() ) {
            $idUser =  $token->user;
            $resultUser = $this->queryBus->ask(new FindUserByIdQuery((int) $idUser));

            $success = $resultUser;

            if($resultUser != null){
                $user = User::fromInfrastructure((object) $resultUser->toArray());
                $passwordEncode = $this->passwordHasher->hashPassword($user, $form->getData()['password']);

                $userCommand = new UpdatePasswordCommand(
                        $user->getId(),
                        $passwordEncode
                );

                $this->commandBus->dispatch($userCommand);

                return $this->redirectToRoute('shared.login');
            }
        }

        return $this->render('@shared/login/change_password.html.twig',
                ['form' => $form,
                'success' => $success]
        );
    }
}