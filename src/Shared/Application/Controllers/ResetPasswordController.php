<?php

namespace App\Shared\Application\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ResetPasswordController
{
    public function __invoke(AuthenticationUtils $authenticationUtils): Response
    {


        return $this->render('@shared/login/index.html.twig', [
                'controller_name' => 'LoginController']);
    }
}