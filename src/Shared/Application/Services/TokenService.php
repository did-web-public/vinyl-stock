<?php

namespace App\Shared\Application\Services;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class TokenService
{
    private  string $key;
    private  string $requestAt;
    private  string $expiresAt;

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    public function generateRefreshToken(int $id, $alg = 'HS256'): string
    {
        $this->requestAt = time();
        $this->expiresAt = $this->requestAt  + 600;

        $payload =[
                'user' => $id,
                'iat' => $this->requestAt,
                'exp' => $this->expiresAt
        ];

        return JWT::encode($payload, $this->key, $alg);
    }

    public function decodeRefreshToken($token, $alg = 'HS256')
    {
        try {

            return JWT::decode($token, new Key($this->key, $alg));
        } catch(\Exception $e) {

            return null;
        }
    }

    public function requestAt(): string
    {
        return $this->requestAt;
    }

    public function expiresAt(): string
    {
        return $this->expiresAt;
    }


}