<?php

namespace App\Shared\Application\Services\Emails;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;

class EmailService
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function senderEmail(string $name, array $data): TemplatedEmail
    {
        try {
            $sender = new $name($this->mailer);

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $sender->senderEmail($data);
    }


}