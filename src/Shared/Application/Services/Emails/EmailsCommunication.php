<?php

namespace App\Shared\Application\Services\Emails;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;

interface EmailsCommunication
{
    function composeTxtMail(string $pathToResetPassword): string;
    function senderEmail(array $data): TemplatedEmail;
}