<?php

namespace App\Shared\Application\Services\Emails;

use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;

class EmailForgetPassword implements EmailsCommunication
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    function composeTxtMail(string $pathToResetPassword): string
    {
        return 'Accede a este link para cambiar tu contraseña: ' . $pathToResetPassword;
    }

    function senderEmail(array $data): TemplatedEmail
    {
        $subject = 'Recordatorio de contraseña ['.$data['version'].']';

        $email = (new TemplatedEmail())
                ->from($data['emailForm'])
                ->to($data['destination'])
                ->subject($subject)
                ->text($this->composeTxtMail($data['urlTarget']))
                ->htmlTemplate('@shared/emails/email_forget_password.html.twig')
                ->context(['pathToResetPassword' => $data['urlTarget']]);
        try {
            $this->mailer->send($email);

        } catch (TransportExceptionInterface $e) {
            throw new \Exception($e->getMessage());
        }

        return $email;
    }
}