<?php

namespace App\Shared\Application\Response;

use App\Shared\Domain\Bus\Query\Response;
use App\Shared\Domain\Model\ResetPassword\ResetPassword;

class ResetPasswordResponse  implements Response
{
    private string $id;
    private int $userId;
    private string $hashedToken;
    private \DateTime $requestAt;
    private \DateTime $expiresAt;

    public function __construct(ResetPassword $resetPassword)
    {
        $this->id = $resetPassword->id();
        $this->userId = $resetPassword->userId();
        $this->hashedToken = $resetPassword->hashedToken();
        $this->requestAt = $resetPassword->requestAt();
        $this->expiresAt = $resetPassword->expiresAt();
    }

    public function id(): string
    {
        return $this->id;
    }

    public function userId(): int
    {
        return $this->userId;
    }

    public function hashedToken(): string
    {
        return $this->hashedToken;
    }

    public function requestAt(): \DateTime
    {
        return $this->requestAt;
    }

    public function expiresAt(): \DateTime
    {
        return $this->expiresAt;
    }
}