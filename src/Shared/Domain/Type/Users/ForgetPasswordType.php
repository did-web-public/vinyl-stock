<?php

namespace App\Shared\Domain\Type\Users;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ForgetPasswordType extends AbstractType
{
    public function buildform(FormBuilderInterface $builder, array $options): void
    {
        $builder
                ->add('email', EmailType::class)
                ->add('save', SubmitType::class, ['label' => 'Enviar para restablecer password']);

    }
}