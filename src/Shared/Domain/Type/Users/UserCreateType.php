<?php

namespace App\Shared\Domain\Type\Users;

use App\Shared\Domain\Model\Users\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserCreateType extends AbstractType
{
    public function buildform(FormBuilderInterface $builder, array $options): void
    {
        $builder
                ->add('email', EmailType::class)
                ->add('name', TextType::class)
                ->add('roles', ChoiceType::class, [
                        'multiple' => true,
                        'expanded' => false,
                        'choice_attr' => ['class' => 'form-control'],
                        'choices' => [
                                'ROLE_ADMIN' => 'ROLE_ADMIN',
                                'ROLE_USER' => 'ROLE_USER'
                        ]
                ])
                ->add('password', TextType::class)
                ->add('save', SubmitType::class, ['label' => 'Crear Usuario']);

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                'data_class' => User::class,
        ]);
    }
}