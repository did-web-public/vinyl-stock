<?php

namespace App\Shared\Domain\Type\ResetPassword;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ChangePasswordType extends AbstractType
{
    public function buildform(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('password', RepeatedType::class,
                                ['type' => PasswordType::class,
                                'invalid_message' => 'The password fields must match.',
                                'options' => array('attr' => array('class' => 'password-field')),
                                'required' => true,
                                'first_options'  => array('label' => 'Password'),
                                'second_options' => array('label' => 'Repite Password')])

                ->add('save', SubmitType::class, ['label' => 'Restablecer Password']);
    }
}