<?php

namespace App\Shared\Domain\Model\Users;


use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, PasswordAuthenticatedUserInterface
{

    private int $id;
    private string $email;
    private array $roles = [];
    private string $password;
    private string $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $passwordEncoded): self
    {
        $this->password = $passwordEncoded;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUserIdentifier(): string
    {
        return $this->email;
    }

    public static function fromArray(?array $array): ?self
    {
        $user = null;
        if($array != null) {
            $user = new User();
            $user->setId($array['id']);
            $user->setEmail($array['email']);
            $user->setRoles($array['roles']);

            if (isset($array['password'])) {
                $user->setPassword($array['password']);
            }
            $user->setName($array['name']);
        }
        return $user;
    }

    public static function fromInfrastructure(object $userObject): self
    {
        $user = new User();
        $user->setId($userObject->id);
        $user->setEmail($userObject->email);
        $user->setRoles($userObject->roles);
        $user->setPassword($userObject->password);
        $user->setName($userObject->name);

        return $user;
    }

    public static function toArray(User $user): array
    {
        return [
                'id' => $user->getId(),
                'email' => $user->getEmail(),
                'roles' => $user->getRoles(),
                'password' => $user->getPassword(),
                'name' => $user->getName()
        ];
    }

}