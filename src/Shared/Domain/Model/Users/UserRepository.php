<?php

namespace App\Shared\Domain\Model\Users;

use App\Office\Application\Response\Users\UserCollectionResponse;
use App\Office\Application\Response\Users\UserResponse;
use App\Shared\Domain\Pagination;

interface UserRepository
{
    public function findAll(Pagination $pagination): UserCollectionResponse;
    public function findUserById(int $id): ?UserResponse;
    public function updateUser(User $user): void;
    public function createUser(User $user);
    public function deleteUser($id): bool;
    public function findUserByEmail(string $email): ?UserResponse;
}