<?php

namespace App\Shared\Domain\Model\Users;

use App\Shared\Domain\Collection\ObjectCollection;

class UsersCollection extends ObjectCollection
{

    protected function className(): string
    {
        return User::class;
    }

    public function toArray(): array
    {
        $convertToArray = [];
        foreach($this->getCollection() as $items) {
            $convertToArray[] = User::toArray($items);
        }
        return $convertToArray;
    }

    public  function fromArray(array $arrays): void
    {
        foreach ($arrays as $array) {
            $this->add(User::fromArray($array));
        }
    }


    public function self(): self
    {
        return $this;
    }
}