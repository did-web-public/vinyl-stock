<?php

namespace App\Shared\Domain\Model\ResetPassword;

use App\Shared\Domain\Model\Users\User;

interface ResetPasswordRepository
{
    public function save(ResetPassword $resetPassword): void;
    public function updatePassword(User $user): void;
}