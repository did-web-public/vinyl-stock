<?php

namespace App\Shared\Domain\Model\ResetPassword;

use App\Shared\Application\Services\TokenService;
use App\Shared\Domain\Model\ResetPassword\ValueObjects\ResetPasswordId;
use Ramsey\Uuid\Nonstandard\Uuid;

class ResetPassword
{
    private ?ResetPasswordId $id;
    private int $userId;
    private string $hashedToken;
    private int $requestAt;
    private int $expiresAt;

    public function __construct(
            ?ResetPasswordId $id,
            int $userId,
            ?string $hashedToken = null,
            int $requestAt = null,
            int $expiresAt = null
    )
    {
        $this->id = $id ?? ResetPasswordId::create(Uuid::uuid4());
        $this->userId = $userId;
        $this->requestAt = $requestAt;
        $this->expiresAt = $expiresAt;
        $this->hashedToken = $hashedToken;
    }

    public function id(): ResetPasswordId
    {
        return $this->id;
    }

    public function userId(): int
    {
        return $this->userId;
    }

    public function hashedToken(): string
    {
        return $this->hashedToken;
    }

    public function requestAt(): int
    {
        return $this->requestAt;
    }

    public function expiresAt(): int
    {
        return $this->expiresAt;
    }
}