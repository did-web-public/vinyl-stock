<?php

namespace App\Shared\Infrastructure\Helper;

use Faker\Factory;

final class Faker
{
    private const LANG = 'es_ES';

    public static function number(int $nbDigits = null, bool $strict = false): int
    {
        return Factory::create(self::LANG)->randomNumber($nbDigits, $strict);
    }

    public static function email(): string
    {
        return Factory::create(self::LANG)->email;
    }

    public static function firstName(): string
    {
        return Factory::create(self::LANG)->firstName;
    }

    public static function lastName(): string
    {
        return Factory::create(self::LANG)->lastName;
    }

    public static function word(): string
    {
        return Factory::create(self::LANG)->word();
    }
}