<?php

namespace App\Shared\Infrastructure\Repository;

use App\Shared\Domain\Model\ResetPassword\ResetPassword;
use App\Shared\Domain\Model\ResetPassword\ResetPasswordRepository;
use App\Shared\Domain\Model\Users\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class DoctrineResetPasswordRepository implements ResetPasswordRepository
{
    private EntityManagerInterface $em;
    private ObjectRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(ResetPassword::class);
    }

    public function save(ResetPassword $resetPassword): void
    {
        $this->em->persist($resetPassword);
        $this->em->flush();
    }

    public function updatePassword(User $user): void
    {
        $queryBuilder = $this->em->createQueryBuilder();

        $query = $queryBuilder->update(User::class, 'u')
                ->set('u.password', ':password')
                ->where('u.id = :id')
                ->setParameter('password', $user->getPassword())
                ->setParameter('id', $user->getId())
                ->getQuery();

        $query->execute();
    }
}