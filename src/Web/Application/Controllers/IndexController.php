<?php

namespace App\Web\Application\Controllers;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends AbstractController
{
    public function __invoke(): Response
    {
        return $this->render('@web/index.html.twig', ['Var1'=>'Hola']);
    }
}