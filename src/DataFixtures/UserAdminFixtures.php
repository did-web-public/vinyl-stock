<?php

namespace App\DataFixtures;

use App\Shared\Domain\Model\Users\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserAdminFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager)
    {
        $userSuperAdmin = new User();
        $userSuperAdmin->setEmail('info@did-web.com');
        $password = $this->hasher->hashPassword($userSuperAdmin , '12345678');
        $userSuperAdmin->setPassword($password);
        $userSuperAdmin->setRoles(['ROLE_SUPER_ADMIN']);

        $manager->persist($userSuperAdmin);
        $manager->flush();
    }
}