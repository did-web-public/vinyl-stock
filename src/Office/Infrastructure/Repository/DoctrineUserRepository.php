<?php

namespace App\Office\Infrastructure\Repository;


use App\Office\Application\Response\Users\UserCollectionResponse;
use App\Office\Application\Response\Users\UserResponse;
use App\Shared\Domain\Model\Users\User;
use App\Shared\Domain\Model\Users\UserRepository;
use App\Shared\Domain\Model\Users\UsersCollection;
use App\Shared\Domain\Pagination;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ObjectRepository;
use Exception;

class DoctrineUserRepository implements UserRepository
{
    private EntityManagerInterface $em;
    private ObjectRepository $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(User::class);
    }

    public function findAll(Pagination $pagination): UserCollectionResponse
    {
        $query = $this->repository->createQueryBuilder('u')
                ->orderBy('u.name', 'ASC')
                ->getQuery();

        $paginator = new Paginator($query);

        $paginator->getQuery()
                ->setFirstResult($pagination->offset())
                ->setMaxResults($pagination->limit());

        $pagination->updateTotals($paginator->count());

        return $this->getUserCollectionResponse($paginator, $pagination);
    }

    private function getUserCollectionResponse($userFind, ?Pagination $pagination = null): UserCollectionResponse
    {
        $userCollection = UsersCollection::init();

        if(!empty($userFind)) {
            foreach ($userFind as $user) {
                $userDomain = User::fromInfrastructure($user);
                $userResponse = new UserResponse($userDomain);
                $userCollection->add($userResponse);
            }
        }

        return new UserCollectionResponse($userCollection, $pagination);
    }

    public function findUserById(int $id): ?UserResponse
    {
        $resultUser = $this->repository->findOneBy(['id' => $id]);
        if (empty($resultUser)) {
            return null;
        }

        $userDomain = User::fromInfrastructure($resultUser);

        return new UserResponse($userDomain);
    }

    public function updateUser(User $user): void
    {
        $queryBuilder = $this->em->createQueryBuilder();

        $query = $queryBuilder->update(User::class, 'u')
            ->set('u.email', ':email')
            ->set('u.name', ':name')
            ->set('u.roles', ':roles')
            ->where('u.id = :id')
            ->setParameter('email', $user->getEmail())
            ->setParameter('name', $user->getName())
            ->setParameter('roles', json_encode($user->getRoles()))
            ->setParameter('id', $user->getId())
            ->getQuery();

        $query->execute();

    }

    public function createUser(User $user)
    {
        $this->em->persist($user);
        $this->em->flush();
    }

    public function deleteUser($id): bool
    {
        $userToDelete = $this->repository->findOneBy(['id' => $id]);
        if (!$userToDelete) {
            echo new \Exception('No User found for id '.$id);
            return false;
        }

        $this->em->remove($userToDelete);
        $this->em->flush();
        return true;
    }

    public function findUserByEmail(string $email): ?UserResponse
    {
        $resultUser = $this->repository->findOneBy(['email' => $email]);
        if (empty($resultUser)) {
            return null;
        }

        $userDomain = User::fromInfrastructure($resultUser);

        return new UserResponse($userDomain);
    }

}