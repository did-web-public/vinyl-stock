<?php

namespace App\Office\Application\Controllers\Users;

use App\Office\Application\Query\Users\FindAllUsersQuery;
use App\Shared\Domain\Bus\Query\QueryBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ListUsersController extends AbstractController
{
    private QueryBus $queryBus;

    function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function __invoke(Request $request): Response
    {
        $page = $request->get('page', 1);
        $pagination = ['page' => $page];
        $allUsers = $this->queryBus->ask(new FindAllUsersQuery($pagination));

        return $this->render('@office/Users/list.html.twig', ['allUsers' => $allUsers]);
    }
}