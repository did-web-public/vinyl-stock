<?php

namespace App\Office\Application\Controllers\Users;

use App\Office\Application\Command\Users\EditUserCommand;
use App\Office\Application\Query\Users\FindUserByIdQuery;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\Model\Users\User;
use App\Shared\Domain\Type\Users\UserEditType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EditUserController extends AbstractController
{
    private QueryBus $queryBus;
    private CommandBus $commandBus;

    function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function __invoke(Request $request): Response
    {
        $idUser =  $request->get('id');
        $resultUser = $this->queryBus->ask(new FindUserByIdQuery($idUser));
        $user =  User::fromArray($resultUser->toArray());
        $form = $this->createForm(UserEditType::class, $user, [
                'action' => $this->generateUrl('office.users.edit', ['id' => $idUser]),
                'method' => 'POST'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userCommand = new EditUserCommand(
                    $form->getData()->getId(),
                    $form->getData()->getEmail(),
                    $form->getData()->getName(),
                    $form->getData()->getRoles()
            );

            $this->commandBus->dispatch($userCommand);

            return $this->redirectToRoute('office.users.list');
        }

        return $this->render('@office/Users/edit.html.twig',['form' => $form]);
    }
}