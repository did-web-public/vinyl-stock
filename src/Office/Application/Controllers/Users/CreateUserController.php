<?php

namespace App\Office\Application\Controllers\Users;

use App\Office\Application\Command\Users\CreateUserCommand;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Model\Users\User;
use App\Shared\Domain\Type\Users\UserCreateType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateUserController extends AbstractController
{
    private CommandBus $commandBus;
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(CommandBus $commandBus, UserPasswordHasherInterface $passwordHasher)
    {
        $this->commandBus = $commandBus;
        $this->passwordHasher = $passwordHasher;
    }

    public function __invoke(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserCreateType::class, $user, [
                'action' => $this->generateUrl('office.users.create'),
                'method' => 'POST'
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $password = $this->passwordHasher->hashPassword($user, $form->getData()->getPassword());

            $userCommand = new CreateUserCommand(
                    $form->getData()->getEmail(),
                    $form->getData()->getName(),
                    $form->getData()->getRoles(),
                    $password
            );

            $this->commandBus->dispatch($userCommand);

            return $this->redirectToRoute('office.users.list');
        }

        return $this->render('@office/Users/create.html.twig',['form' => $form]);
    }
}