<?php

namespace App\Office\Application\Controllers\Users;

use App\Office\Application\Command\Users\DeleteUserCommand;
use App\Office\Application\Query\Users\FindUserByIdQuery;
use App\Shared\Domain\Bus\Command\CommandBus;
use App\Shared\Domain\Bus\Query\QueryBus;
use App\Shared\Domain\Model\Users\User;
use App\Shared\Domain\Type\Users\UserDeleteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DeleteUserController extends AbstractController
{
    private QueryBus $queryBus;
    private CommandBus $commandBus;

    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function __invoke(Request $request): Response
    {
        $idUser =  $request->get('id');
        $resultUser = $this->queryBus->ask(new FindUserByIdQuery($idUser));
        $user =  User::fromArray($resultUser->toArray());
        $form = $this->createForm(UserDeleteType::class, $user, [
                'action' => $this->generateUrl('office.users.delete', ['id' => $idUser]),
                'method' => 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userCommand = new DeleteUserCommand($form->getData()->getId());

            $this->commandBus->dispatch($userCommand);

            return $this->redirectToRoute('office.users.list');
        }

        return $this->render('@office/Users/delete.html.twig',['form' => $form, 'user' => $user]);



    }

}