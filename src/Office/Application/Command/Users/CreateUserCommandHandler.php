<?php

namespace App\Office\Application\Command\Users;

use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\Model\Users\User;
use App\Shared\Domain\Model\Users\UserRepository;

class CreateUserCommandHandler  implements CommandHandler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(CreateUserCommand $command): void
    {
        $user =  new User();
        $user->setEmail($command->email());
        $user->setName($command->name());
        $user->setRoles($command->roles());
        $user->setPassword($command->password());

        $this->userRepository->createUser($user);
    }
}