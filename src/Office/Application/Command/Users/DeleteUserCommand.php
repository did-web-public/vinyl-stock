<?php

namespace App\Office\Application\Command\Users;

use App\Shared\Domain\Bus\Command\Command;

class DeleteUserCommand implements Command
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function id(): int
    {
        return $this->id;
    }
}