<?php

namespace App\Office\Application\Command\Users;

use App\Shared\Domain\Bus\Command\Command;

class CreateUserCommand implements Command
{
    private string $email;
    private string $name;
    private array $roles;
    private string $password;

    public function __construct(string $email, string $name, array $roles, string $password)
    {
        $this->email = $email;
        $this->name = $name;
        $this->roles = $roles;
        $this->password = $password;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function roles(): array
    {
        return $this->roles;
    }

    public function password(): string
    {
        return $this->password;
    }
}