<?php

namespace App\Office\Application\Command\Users;

use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\Model\Users\UserRepository;

class DeleteUserCommandHandler  implements CommandHandler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(DeleteUserCommand $command): bool
    {
        try {
            return $this->userRepository->deleteUser($command->id());

        } catch (\Exception $e ){
            echo $e->getMessage();
        }

        return false;
    }
}