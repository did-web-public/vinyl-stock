<?php

namespace App\Office\Application\Command\Users;

use App\Office\Application\Response\Users\UserResponse;
use App\Shared\Domain\Bus\Command\CommandHandler;
use App\Shared\Domain\Model\Users\User;
use App\Shared\Domain\Model\Users\UserRepository;

class EditUserCommandHandler implements CommandHandler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(EditUserCommand $command): void
    {
        $user =  new User();
        $user->setId($command->id());
        $user->setEmail($command->email());
        $user->setName($command->name());
        $user->setRoles($command->roles());

        $this->userRepository->updateUser($user);
    }
}