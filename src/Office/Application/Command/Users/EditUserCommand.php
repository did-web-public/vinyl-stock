<?php

namespace App\Office\Application\Command\Users;

use App\Shared\Domain\Bus\Command\Command;

class EditUserCommand implements Command
{
    private int $id;
    private string $email;
    private string $name;
    private array $roles;

    public function __construct(int $id, string $email, string $name, array $roles)
    {
        $this->id = $id;
        $this->email = $email;
        $this->name = $name;
        $this->roles = $roles;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function roles(): array|string
    {
        return $this->roles;
    }
}