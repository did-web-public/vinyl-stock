<?php

namespace App\Office\Application\Query\Users;

use App\Shared\Domain\Bus\Query\Query;

class FindUserByIdQuery implements Query
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function id(): int
    {
        return $this->id;
    }
}