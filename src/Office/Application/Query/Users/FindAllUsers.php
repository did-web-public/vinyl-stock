<?php

namespace App\Office\Application\Query\Users;

use App\Office\Application\Response\Users\UserCollectionResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Model\Users\UserRepository;
use App\Shared\Domain\Pagination;

class FindAllUsers implements QueryHandler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(FindAllUsersQuery $query): UserCollectionResponse
    {
        $pagination = new Pagination($query->pagination()['page']);
        return $this->userRepository->findAll($pagination);
    }
}