<?php

namespace App\Office\Application\Query\Users;

use App\Shared\Domain\Bus\Query\Query;

class FindAllUsersQuery implements Query
{
    private array $pagination;

    public function __construct(array $pagination)
    {
        $this->pagination = $pagination;
    }

    public function pagination(): array
    {
        return $this->pagination;
    }
}