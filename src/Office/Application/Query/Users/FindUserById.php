<?php

namespace App\Office\Application\Query\Users;

use App\Office\Application\Response\Users\UserResponse;
use App\Shared\Domain\Bus\Query\QueryHandler;
use App\Shared\Domain\Model\Users\UserRepository;

class FindUserById implements QueryHandler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(FindUserByIdQuery $query): ?UserResponse
    {
        return $this->userRepository->findUserById($query->id());
    }
}