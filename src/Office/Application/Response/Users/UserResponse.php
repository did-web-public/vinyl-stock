<?php

namespace App\Office\Application\Response\Users;

use App\Shared\Domain\Bus\Query\Response;
use App\Shared\Domain\Model\Users\User;

class UserResponse  implements Response
{
    private int $id;
    private string $email;
    private array $roles;
    private string $name;
    private string $password;

    public function __construct(User $user)
    {
        $this->id = $user->getId();
        $this->email = $user->getEmail();
        $this->roles = $user->getRoles();
        $this->name = $user->getName();
        $this->password = $user->getPassword();
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function roles(): array
    {
        return $this->roles;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function toArray(): array
    {
        return [
          'id' => $this->id(),
          'email' => $this->email(),
          'roles' => $this->roles(),
          'name' => $this->name(),
          'password' => $this->password(),
        ];
    }
}