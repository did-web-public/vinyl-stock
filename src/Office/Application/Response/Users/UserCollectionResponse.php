<?php

namespace App\Office\Application\Response\Users;

use App\Shared\Domain\Bus\Query\Response;
use App\Shared\Domain\Model\Users\UsersCollection;
use App\Shared\Domain\Pagination;

class UserCollectionResponse implements Response
{
    private array $users;
    private ?Pagination $pagination;


    public function __construct(UsersCollection $userCollection, ?Pagination $pagination = null)
    {
        $this->users = [];
        foreach($userCollection->getCollection() as $user) {
            $this->users[] = $user;
        }
        $this->pagination = ($pagination != null)? $pagination : null;
    }

    public function users(): array
    {
        return $this->users;
    }

    public function pagination(): ?Pagination
    {
        return $this->pagination;
    }

    public function toArray()
    {
        return array_map(function ($user){
            return $user->toArray();
        }, $this->users());
    }

    public function count()
    {
        return count($this->users);
    }

}